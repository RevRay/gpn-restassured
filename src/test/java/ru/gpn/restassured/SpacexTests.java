package ru.gpn.restassured;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;

public class SpacexTests {

    public RequestSpecification reqSpec;
    public ResponseSpecification respSpec;

    @BeforeClass
    public void setUpClass() {
        baseURI = "https://api.spacexdata.com/v3";
        filters(
                new RequestLoggingFilter(), new ResponseLoggingFilter()
        );

        respSpec = new ResponseSpecBuilder()
                .expectContentType(ContentType.JSON)
                .expectStatusCode(200)
                .build();

        reqSpec = new RequestSpecBuilder()
                .addHeader("random", "123")
                .addHeader("random2", "12dsff3")
                .addHeader("random3", "12333")
                .build();
    }

    //RequestSpecification & ResponseSpecification

    @Test
    public void getVehiclesData() {
        given() //precondition   0
                .spec(reqSpec)
                .when() //feature, logic  1
//                .log().all()
                .get("/roadster")
                .then() //assert  2
                .spec(respSpec)
//                .log().all()
//                .statusCode(200)
//                .contentType(ContentType.JSON)
                .body("name", Matchers.equalTo("Elon Musk's Tesla Roadster"))
                .body("name", Matchers.equalTo("Elon Musk's Tesla Roadster"))
                .body("name", Matchers.equalTo("Elon Musk's Tesla Roadster"));
    }

    @Test
    public void getCompanyHistory() {
        given() //precondition   0
                .spec(reqSpec)
                .when() //feature, logic  1
//                .log().all()
                .get("/history")
                .then() //assert  2
//                .log().all()
//                .statusCode(200)
//                .contentType(ContentType.JSON);
                .spec(respSpec);
    }

    @Test
    public void getCompanyHistorySingleEntry() {
        given() //precondition   0
                .queryParam("id", "1")
                .when() //feature, logic  1
//                .log().all()
                .get("/history")
                .then() //assert  2
//                .log().all()
                .spec(respSpec);

    }
    @Test
    public void getCapsules() {
        given() //precondition   0
                .queryParam("id", "1")
                .when() //feature, logic  1
//                .log().all()
                .get("/capsules/C112")
                .then() //assert  2
//                .log().all()
                .spec(respSpec)
                .body("capsule_serial", Matchers.equalTo("C112"))
                .body("missions[0].name", Matchers.containsString("S-1"));

    }

    //JSON JsonPath
    //GPath 95%

    @Test
    public void getCapsules2() {
        JsonPath capsuleData = given() //precondition   0
                .queryParam("id", "1")
                .when() //feature, logic  1
//                .log().all()
                .get("/capsules/C112")
                .then() //assert  2
//                .log().all()
                .extract().body().jsonPath();


    }

}
